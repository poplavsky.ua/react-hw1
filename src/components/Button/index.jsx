import { Component } from "react";
import './button.scss';

class Button extends Component {
  render() {
    const { backgroundColor, text, onClick, className } = this.props;
    return (
      <>
        <button
          styles={{ backgroundColor: backgroundColor }}
          className={className}
          onClick={onClick}
        >
          {text}
        </button>
      </>
    );
  }
}

export default Button;