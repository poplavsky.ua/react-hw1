// import style from './modal.scss';
import { Component } from "react";
import Button from "../Button";
import "./modal.scss";

class Modal extends Component {
  render() {
    const { header, closeButton, text, actions } = this.props;

    return (
      <>
        <div className="overlay" onClick={actions}></div>
        <div className="modal">
          <div className="header">
            <h3 className="title_header">{header}</h3>
            {closeButton && (
              <Button className={"close_symbol"} text="✕" onClick={actions} />
            )}
          </div>

          <div className="modal_text">
            <p>{text}</p>
          </div>

          <div className="modal_btns">
            <Button className={"btn modal_btn"} text="Ok" onClick={actions} />
            <Button
              className={"btn modal_btn"}
              text="Cancel"
              onClick={actions}
            />
          </div>
        </div>
      </>
    );
  }
}

export default Modal;
