import { Component } from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenedFirstModal: false,
      isOpenedSecondModal: false,
    };
  }

  openFirstModal = () => {
    this.setState({ isOpenedFirstModal: true });
  };
  openSecondModal = () => {
    this.setState({ isOpenedSecondModal: true });
  };
  closeFirstModal = () => {
    this.setState({ isOpenedFirstModal: false });
  };
  closeSecondModal = () => {
    this.setState({ isOpenedSecondModal: false });
  };

  render() {
    const { isOpenedFirstModal, isOpenedSecondModal } = this.state;
    return (
      <>
        <div className="btn_container">
          <Button
            className={"btn btn_first"}
            backgroundColor={"red"}
            text="Open first modal"
            onClick={this.openFirstModal}
          />
          <Button
            className={"btn btn_second"}
            backgroundColor={"green"}
            text="Open second modal"
            onClick={this.openSecondModal}
          />
        </div>

        {isOpenedFirstModal && <Modal 
            header='Do you want to delete this file?' 
            closeButton={true} 
            text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?" 
            actions={this.closeFirstModal}
        />}

        {isOpenedSecondModal && <Modal 
            header='Are you sure you want to cencel?' 
            closeButton={true} 
            text="If you cancel now you will not receive this file"
            actions={this.closeSecondModal}
        />}
      </>
    );
  }
}

export default App;